from __future__ import annotations

import dataclasses

import strawberry
import uvicorn
from fastapi import FastAPI
from starlette.requests import Request
from starlette.responses import Response
from starlette.websockets import WebSocket
from strawberry.asgi import GraphQL
from strawberry.types import Info as StrawberryInfo


@dataclasses.dataclass
class Loaders:
    pass


@dataclasses.dataclass
class Context:
    request: Request | WebSocket
    response: Response | None
    loaders: Loaders


class Info(StrawberryInfo[Context, None]):
    pass


class GraphQLApp(GraphQL):
    async def get_context(
        self,
        request: Request | WebSocket,
        response: Response | None = None,
    ) -> Context:
        return Context(
            request=request,
            response=response,
            loaders=Loaders(),
        )


@strawberry.type
class Query:
    @strawberry.field
    async def the_answer_to_the_ultimate_question_of_life_the_universe_and_everything(
        self,
        info: Info,
    ) -> int:
        return 42


def create_gql_app() -> GraphQL:
    schema = strawberry.Schema(query=Query)
    graphql_app = GraphQLApp(schema=schema)
    return graphql_app


def create_app():
    app = FastAPI()
    app.mount("/graphql/", create_gql_app())
    return app


if __name__ == '__main__':
    uvicorn.run("main:create_app", factory=True, reload=True)
