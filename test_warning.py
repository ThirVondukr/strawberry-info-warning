import pytest


def test_warning():
    with pytest.warns(
        match="Argument name-based matching of 'info' is deprecated and will be removed in v1.0."
    ):
        import main  # noqa
